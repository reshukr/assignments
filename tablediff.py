class GenarateDbDiff(object):
    fmisprod =  {} #fields of table present in stage but not in prod
    fmisstage = {} #fields of table present in prod but not in stage
    tbmisprod = {} # tables present in stage but not in prod db
    tbmisstage= {} # tables present in prod but not in stage db
    diffields = {} #fields present in both the tables but modified
    def __init__(self,dicta,dictb):
        self.dicta = dicta
        self.dictb = dictb
    def compare(self,table):
        self.fieldsfirst = set("".join(j[0]) for j in self.dicta[table])
        self.fieldssecond = set("".join(j[0]) for j in self.dictb[table])
        cls.fmisprod[table] =  list(self.fieldsfirst - self.fieldssecond)
        cls.fmisstage[table] =  list(self.fieldssecond - self.fieldsfirst)
        if cls.fmisprod[table] == []:#delete emplty lists
            del cls.fmisprod[table]
        if cls.fmisstage[table] == []:
            del cls.fmisstage[table]

    #dicta = {u'dept': [(u'name', u'varchar(10)', u'YES', u'PRI', None, u''), (u'branch', u'varchar(15)', u'YES', u'', None, u''), (u'city', u'varchar(10)', u'YES', u'', None, u'')],u'person': [(u'friend', u'varchar(5)', u'NO', u'', None, u''),(u'sno', u'varchar(5)', u'NO', u'PRI', None, u''), (u'name', u'varchar(30)', u'YES', u'', None, u''), (u'age', u'int(3)', u'YES', u'', None, u''), (u'sex', u'varchar(1)', u'YES', u'FK', None, u'')], u'office': [(u'name', u'varchar(18)', u'YES', u'', None, u''), (u'location', u'varchar(15)', u'YES', u'', None, u''), (u'city', u'varchar(15)', u'YES', u'', None, u'')]}
    #dictb = {u'social': [(u'nickname', u'varchar(5)', u'NO', u'PRI', None, u''),(u'sno', u'varchar(4)', u'NO', u'PRI', None, u''), (u'name', u'varchar(30)', u'YES', u'', None, u''), (u'age', u'int(3)', u'YES', u'', None, u'')],u'person': [(u'nickname', u'varchar(5)', u'NO', u'PRI', None, u''),(u'sno', u'varchar(4)', u'NO', u'PRI', None, u''), (u'name', u'varchar(30)', u'YES', u'', None, u''), (u'age', u'int(3)', u'YES', u'', None, u'')], u'office': [(u'name', u'varchar(10)', u'YES', u'', None, u''), (u'location', u'varchar(15)', u'YES', u'', None, u''), (u'city', u'varchar(10)', u'YES', u'', None, u'')]}

    def comparefields(self,tablename,stage,prod):
        self.res = []
        self.indexa = ["".join(p[0]) for p in self.stage]
        indexb = ["".join(p[0]) for p in self.prod]
        for count, i in enumerate(self.indexa):
            try:
                self.indb = self.indexb.index(i)
                if self.stage[count] != self.prod[indb]:
                    self.res.append(stage[count])
            except ValueError:
                continue

        cls.diffields[tablename] = self.res


    # tables present in stage but not in prod db
    for i in self.dicta:
        if i not in self.dictb:
            cls.tbmisprod[i]= self.dicta[i]
    for i in tbmisprod:
        del dicta[i]
    # tables present in prod but not in stage db
    for i in dictb:
        if i not in dicta:
            tbmisstage[i]= dictb[i]
    for i in tbmisstage:
        del dictb[i]
    #fields of table present in stage but not in prod and vice versa
    for table in dicta:
        compare(table)
    # fields of table present in both but types are  different
    for tablename in dicta:
        comparefields(tablename,dicta[tablename],dictb[tablename])

    #print fmisprod
    #print fmisstage
    #print tbmisprod
    #print tbmisstage
    print diffields
    def generatesql():
        f = open("output.sql",'w')
        #create tables present in stage but not in prod
        if tbmisprod != {}:
            for table in  tbmisprod:
                statement = "create table"
                statement += " " + table + " ( "
                for i in tbmisprod[table]:
                    statement +=  i[0] + ' ' + i[1]
                    if "PRI" in i :
                        statement+= " Primary key,"
                    elif "Foreign" in i :
                        statement += " Foreign key,"
                    else:
                        statement+= ","
                statement = statement.strip(",")
                statement += ');'
                f.write(statement)
                f.write("\n")
                #print statement
                statement =""
        #delete tables present in prod but not stage
        if tbmisstage != {}:
            for table in tbmisstage:
                statement = "Drop table "
                statement+= table  + ';'
                f.write(statement)
                f.write("\n")
                #print statement
                statement = " "
        #Add fields present in stage but missing in production


        if fmisprod !={}:
            for table in fmisprod:

                for field in fmisprod[table]:

                    for i in dicta[table]:

                        if i[0] == field:
                            #print field
                            #print i[0]
                            statement = "Alter table " + table + ' add ' + field + ' '+ i[1]+''
                            if i[3]=="PRI":
                                statement += " Primary key ;"
                            elif i[3]=="FK":
                                statement+= " Foreign Key ;"
                            else:
                                statement += ';'
                            f.write(statement)
                            f.write("\n")
        #Delete fields present in prod but not in stage
        for table in fmisstage:
            for fields in fmisstage[table]:
                statement = "Alter table " + table +' drop ' + fields+";"
            f.write(statement)
            f.write("\n")
        #modify fields which are of different type
        for table in diffields:
            for fields in diffields[table]:
                statement =  "ALter table " + table + " modify column " + fields[0] + ' ' + fields[1]
                if fields[3] == "PRI":
                    statement+= " Primary key;"
                elif fields[3] =="Fk":
                    statement+= "Foreign Key;"
                else:
                    statement+=';'
                f.write(statement)
                f.write("\n")



        f.close()
    generatesql()